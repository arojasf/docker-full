//  repository.js
//
//  Exposes a single function - 'connect', which returns
//  a connected repository. Call 'disconnect' on this object when you're done.
'use strict';

var mysql = require('mysql');

//  Class which holds an open connection to a repository
//  and exposes some simple functions for accessing data.
class Repository {
  constructor(connectionSettings) {
    this.connectionSettings = connectionSettings;
    this.connection = mysql.createConnection(this.connectionSettings);
  }
  updateUser(user){
    if(user &&  typeof user.id === "undefined") {
      throw new Error("Whoops!");
    }
    else
    {
      return new Promise((resolve, reject) => {
        var sql="update directory  set texto=? ,checked=?  where user_id=?";
        this.connection.query(sql ,[user.texto, user.checked,user.id], (err, results) => {
          if(err) {
            this.connection = mysql.createConnection(this.connectionSettings);
            return reject(new Error('An error occured update the users: ' + err));
          }
          if(results){
            resolve(results);
          }
        });

      });
    }
  }
  deleteUser(user){
    if(user &&  typeof user.id === "undefined") {
      throw new Error("Whoops!");
    }
    else
    {
      return new Promise((resolve, reject) => {
        var sql="delete from directory  where  user_id = ? ";
        this.connection.query(sql ,[user.id], (err, results) => {
          if(err) {
            this.connection = mysql.createConnection(this.connectionSettings);
            return reject(new Error('An error occured delete the users: ' + err));
          }
          if(results){
            resolve(results);
          }
        });

      });
    }
  }
  insertUser(user ){
    if(user &&  typeof user.text === "undefined") {
      throw new Error("Whoops!");
    }
    else
    {
      return new Promise((resolve, reject) => {
        var sql='insert into directory ( texto,checked) values (?,?)';
        
        this.connection.query(sql ,[user.text, user.checked], (err, results) => {
          if(err) {
            this.connection = mysql.createConnection(this.connectionSettings);
            return reject(new Error('An error occured getting the users: ' + err));
          }
          if(results){
            
            resolve(results.insertId);
          }
        });

      });
    }         
  }
  getUsers() {
    return new Promise((resolve, reject) => {

      this.connection.query('SELECT user_id, texto, checked FROM directory', (err, results) => {
        if(err) {
          this.connection = mysql.createConnection(this.connectionSettings);
          return reject(new Error('An error occured getting the users: ' + err));
        }

        resolve((results || []).map((user) => {
          return {
            id: user.user_id,
            texto: user.texto,
            checked: user.checked
          };
        }));
      });

    });
  }
  getCuarteles(){
    return new Promise((resolve, reject) => {
      
            this.connection.query('SELECT nombre, variedad FROM cuartel', (err, results) => {
              if(err) {
                this.connection = mysql.createConnection(this.connectionSettings);
                return reject(new Error('An error occured getting the cuarteles: ' + err));
              }
              resolve((results || []).map((cuartel) => {
                return {
                  nombre: cuartel.nombre,
                  variedad: cuartel.variedad
                };
              }));
            });
      
          });
    
  }
  getGrupos(){
    return new Promise((resolve, reject) => {
      
            this.connection.query('SELECT nombre, fecha FROM grupo', (err, results) => {
              if(err) {
                this.connection = mysql.createConnection(this.connectionSettings);
                return reject(new Error('An error occured getting the grupos: ' + err));
              }
              resolve((results || []).map((grupo) => {
                return {
                  nombre: grupo.nombre,
                  fecha: grupo.fecha
                };
              }));
            });
      
          });
    
  }
  getUserByEmail(email) {

    return new Promise((resolve, reject) => {

      //  Fetch the customer.
      this.connection.query('SELECT email, phone_number FROM directory WHERE email = ?', [email], (err, results) => {

        if(err) {
          return reject(new Error('An error occured getting the user: ' + err));
        }

        if(results.length === 0) {
          resolve(undefined);
        } else {
          resolve({
            email: results[0].email,
            phone_number: results[0].phone_number
          });
        }

      });

    });
  }

  disconnect() {
    this.connection.end();
  }
}

//  One and only exported function, returns a connected repo.
module.exports.connect = (connectionSettings) => {
  return new Promise((resolve, reject) => {
    if(!connectionSettings.host) throw new Error("A host must be specified.");
    if(!connectionSettings.user) throw new Error("A user must be specified.");
    if(!connectionSettings.password) throw new Error("A password must be specified.");
    if(!connectionSettings.port) throw new Error("A port must be specified.");

    resolve(new Repository(connectionSettings));
  });
};
