//  users.js
//
//  Defines the users api. Add to a server by calling:
//  require('./users')
'use strict';

//  Only export - adds the API to the app with the given options.
module.exports = (app, options) => {


  
  app.get('/cuartel', (req, res, next) => {
    options.repository.getCuarteles().then((cuarteles) => {
      res.status(200).send(cuarteles.map((cuartel) => { return {
        nombre: cuartel.nombre,
          variedad: cuartel.variedad
        };
      }));
    })
    .catch(next);
  });
  app.get('/grupo', (req, res, next) => {
    options.repository.getGrupos().then((grupos) => {
      res.status(200).send(grupos.map((grupo) => { return {
        nombre: grupo.nombre,
        fecha: grupo.fecha
        };
      }));
    })
    .catch(next);
  });


  // app.get('/users', (req, res, next) => {
  //   options.repository.getUsers().then((users) => {
  //     res.status(200).send(users.map((user) => { return {
  //         email: user.email,
  //         phoneNumber: user.phone_number
  //       };
  //     }));
  //   })
  //   .catch(next);
  // });

  app.get('/users', (req, res, next) => {
    options.repository.getUsers().then((users) => {
      res.status(200).send(users.map((user) => { return {
          id: user.id,
          text: user.texto,
          checked: user.checked
        };
      }));
    })
    .catch(next);
  });
  app.post('/users', (req, res, next) => {
    console.dir(req.body);

    options.repository.insertUser(req.body).then((userId) => {
      res.json( 
        userId
      );
    })
    .catch(next);
  });
  app.delete('/users', (req, res, next) => {
    console.dir(req.body);

    options.repository.deleteUser(req.body).then((affectedRows) => {
      res.json( 
        affectedRows
      );
    })
    .catch(next);
  });
  app.put('/users', (req, res, next) => {
    console.dir(req.body);

    options.repository.updateUser(req.body).then((affectedRows) => {
      res.json( 
        affectedRows
      );
    })
    .catch(next);
  });

  app.get('/search', (req, res, next) => {

    //  Get the email.
    var email = req.query.email;
    if (!email) {
      throw new Error("When searching for a user, the email must be specified, e.g: '/search?email=homer@thesimpsons.com'.");
    }

    //  Get the user from the repo.
    options.repository.getUserByEmail(email).then((user) => {

      if(!user) { 
        res.status(404).send('User not found.');
      } else {
        res.status(200).send({
          email: user.email,
          phoneNumber: user.phone_number
        });
      }
    })
    .catch(next);

  });
};
