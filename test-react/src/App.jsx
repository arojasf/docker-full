import React, { Component } from 'react';
import {Provider} from 'react-redux';

import {configureStore} from 'redux/configureStore';
import * as actions from 'redux/actions';
import Router from 'router';

let store = configureStore();
/*store.subscribe(() => {
	let state = store.getState();
	console.log(state);
});*/
store.dispatch(actions.startItemsList());

class App extends Component {
	render() {
		return (
			<Provider store={store}>
				<div className="App">
					<Router/>
				</div>
			</Provider>
		);
	}
}

export default App;