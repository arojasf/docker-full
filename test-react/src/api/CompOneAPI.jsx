export let filterItemsList = (list,searchText,showChecked) => {
	let filteredList = list.filter((item) => {
		return (
			// Text Search
			((searchText === "") || (item.text.toLowerCase().indexOf(searchText.toLowerCase()) > -1))
			&&
			// Checked
			((showChecked === true) || (!item.checked))
		);
	});
	return filteredList;
};