import React, {Component} from 'react';

class Home extends Component{
	render() {
		return (
			<div className="home">
				<h1>Home</h1>
				<p>Este proyecto utiliza:</p>
				<ul>
					<li>
						<a href="https://github.com/facebookincubator/create-react-app">Create React App (1.4.3)</a>
					</li>
					<li>
						<a href="https://github.com/michaelwayman/node-sass-chokidar">node-sass-chokidar (0.0.3)</a>
					</li>
					<li>
						<a href="https://github.com/mysticatea/npm-run-all">npm-run-all (4.1.2)</a>
					</li>
					<li>
						<a href="https://github.com/ReactTraining/react-router">React Router (4.2.2)</a>
					</li>
					<li>
						<a href="https://redux.js.org/">Redux (3.7.2)</a>
					</li>
					<li>
						<a href="https://github.com/reactjs/react-redux">React Redux (5.0.6)</a>
					</li>
					<li>
						<a href="https://github.com/gaearon/redux-thunk">Redux Thunk (2.2.0)</a>
					</li>
				</ul>
				<p>Comienza a trabajar editando src/App.jsx</p>
			</div>
		);
	}
}

export default Home;