import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as actions from 'redux/actions';

class ListItem extends Component{
	constructor(props){
		super(props);
		this.checkItem = this.checkItem.bind(this);
		this.removeItem = this.removeItem.bind(this);
	}

	checkItem(){
		let {id, checked, text, dispatch} = this.props;
		dispatch(actions.checkItem(id, !checked, text));
	}

	removeItem(e){
		e.preventDefault();
		let {id, dispatch} = this.props;
		dispatch(actions.removeItem(id));
	}

	render(){
		let {text, checked} = this.props;
		let className = checked ? 'list-item checked' : 'list-item';
		return (
			<div className={className}>
				<label>
					<input type="checkbox" checked={checked} onChange={this.checkItem}/>
					<p>{text}</p>
				</label>
				<a href="" className="delete" onClick={this.removeItem}>X</a>
			</div>
		);
	}
}

export default connect()(ListItem);