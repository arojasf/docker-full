import React, {Component} from 'react';
import {connect} from 'react-redux';

import * as actions from 'redux/actions';

class CompOneForm extends Component{
	constructor(props) {
		super(props);
		this.onSubmit = this.onSubmit.bind(this);
	}

	onSubmit(e) {
		e.preventDefault();
		let textValue = this.refs.text.value;

		if(textValue !== ""){
			this.refs.text.value = "";
			this.props.dispatch(actions.startAddItem(textValue));
		}
	}

	render() {
		return (
			<div className="form">
				<form onSubmit={this.onSubmit}>
					<input type="text" ref="text" placeholder="Add item here"/>
					<button className="button">Add</button>
				</form>
			</div>
		);
	}
}

export default connect()(CompOneForm);