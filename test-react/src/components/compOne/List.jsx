import React, {Component} from 'react';
import {connect} from 'react-redux';

import ListItem from 'components/compOne/ListItem';
import {filterItemsList} from 'api/CompOneAPI';

class CompOneList extends Component{
	render() {
		let {itemsList, searchText, showChecked} = this.props;
		let filteredList = filterItemsList(itemsList,searchText,showChecked)
		let renderList = () => {
			return filteredList.map((item) => {
				return (<ListItem key={item.id} {...item}/>);
			});
		};

		return (
			<div className="list">
				{renderList()}
			</div>
		);
	}
}

export default connect(
	(state) => {
		return state;
	}
)(CompOneList);