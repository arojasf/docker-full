// searchText
export let setSearchText = (searchText) => {
	return {
		type: 'SET_SEARCH_TEXT',
		searchText
	}
};

// showChecked
export let setShowChecked = (value) => {
	return {
		type: 'SET_SHOW_CHECKED',
		value
	}
};

// itemsList
export let setItemsList = (itemsList) => {
	return {
		type: 'SET_ITEMS_LIST',
		itemsList
	}
};

export let startItemsList = () => {
	
	return  async(dispatch, getState) => {
		// let tempList = [
		// 	{id: 0, text: 'Item uno', checked: false},
		// 	{id: 1, text: 'Item dos', checked: false},
		// 	{id: 2, text: 'Item tres', checked: true},
		// 	{id: 3, text: 'Item cuatro', checked: true}
		// ];
		// dispatch(setItemsList(tempList));
			fetch('http://localhost:8123/users', {
			  method: 'GET',
			  mode: 'cors',
			  headers: {
				"Access-Control-Allow-Origin": "*",
				"Content-Type": "application/json",
				"Access-Control-Allow-Methods": "GET,PUT,POST,DELETE,PATCH,OPTIONS",
				},
			})
			.then((response) => response.json())
			.then((data) => dispatch(setItemsList(data)))
			.catch((error) => console.log(error));
	};
		
};

export let addItem = (item) => {
	return {
		type: 'ADD_ITEM',
		item
	}	
};

export let startAddItem = (text) => {

	return  async(dispatch, getState) => {
		let newItem = {
			text,
			checked: false
		};

		let data=JSON.stringify(newItem);
		fetch('http://localhost:8123/users', {
					method: 'POST',
					headers: {
						"Content-Type": "application/json",
					},
					"body": data,
				})
		.then((response) => response.json())
		.then( (data)=> {
			newItem.id=data;
			dispatch(addItem(newItem));
		})
		.catch((error) => console.log(error));
		
	}
};


export let removeItem = (id) => { 
	let user={
		"id":id
	};
	
	return  async(dispatch, getState) => {
		fetch('http://localhost:8123/users', {
					method: 'DELETE',
					headers: {
						"Content-Type": "application/json",
					},
					"body": JSON.stringify(user),
				})
				.then((response) => response.json())
				.then( (data)=> {
					return dispatch( {
						type: 'REMOVE_ITEM',
						id
					})
				})
				.catch((error) => console.log(error));
	}
};

export let checkItem = (id, checked, text) => {
	let user={
		id:id,
		checked:checked,
		texto: text
	}
	return  async(dispatch, getState) => {
		fetch('http://localhost:8123/users', {
					method: 'PUT',
					headers: {
						"Content-Type": "application/json",
					},
					"body": JSON.stringify(user),
				})
				.then((response) => response.json())
				.then( (data)=> {
					return dispatch( {
						type: 'CHECK_ITEM',
						id,
						checked
					})
				})
				.catch((error) => console.log(error));
	}
};