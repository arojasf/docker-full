DROP TABLE IF EXISTS `directory`;
DROP TABLE IF EXISTS `cuartel`;
DROP TABLE IF EXISTS `grupo`;

-- create table directory (user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, email TEXT, phone_number TEXT);  
-- insert into directory (email, phone_number) values ('homer@thesimpsons.com', '+1 888 123 1111');  
-- insert into directory (email, phone_number) values ('marge@thesimpsons.com', '+1 888 123 1112');  
-- insert into directory (email, phone_number) values ('maggie@thesimpsons.com', '+1 888 123 1113');  
-- insert into directory (email, phone_number) values ('lisa@thesimpsons.com', '+1 888 123 1114');  
-- insert into directory (email, phone_number) values ('bart@thesimpsons.com', '+1 888 123 1115'); 
create table directory (user_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,texto  text, checked  boolean);  
insert into directory (texto,checked) values ('item 1', true);  
insert into directory (texto, checked) values ('item 2', true);  
insert into directory (texto, checked) values ('item 3', false);  
insert into directory (texto,checked) values ('item 4', true);  
insert into directory (texto, checked) values ('item 5',false); 

create table cuartel (cuartel_id int not null AUTO_INCREMENT PRIMARY KEY, nombre text, variedad text);
insert into cuartel (nombre,variedad) values ('1', 'blanco');
insert into cuartel (nombre,variedad) values ('2', 'blanco');
insert into cuartel (nombre,variedad) values ('3', 'blanco');
insert into cuartel (nombre,variedad) values ('4', 'tinto');
insert into cuartel (nombre,variedad) values ('5', 'tinto');
insert into cuartel (nombre,variedad) values ('6', 'tinto');
insert into cuartel (nombre,variedad) values ('7', 'tinto');

create table grupo (grupo_id int not null AUTO_INCREMENT PRIMARY key, nombre text, fecha date);
insert into grupo (nombre,fecha) values ('1',NOW());
insert into grupo (nombre,fecha) values ('2',NOW());
insert into grupo (nombre,fecha) values ('3',NOW());
